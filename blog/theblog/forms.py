from django import forms
from .models import Post

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'title_tag', 'author', 'body')

        widgets = {
            'title': forms.TextInput(attrs={'class': 'w-80 block mb-8 border-b-2 border-slate-400 py-0.5 px-2 focus:outline-none focus:border-sky-500'}),
            'title_tag': forms.TextInput(attrs={'class': 'w-80 block mb-8 border-b-2 border-slate-400 py-0.5 px-2 focus:outline-none focus:border-sky-500'}),
            'author': forms.Select(attrs={'class': 'w-80 block mb-8 py-0.5 px-2 bg-white border-b-2 border-slate-400 focus:outline-none'}),
            'body': forms.Textarea(attrs={'class': 'w-80 block mb-8 border-2 border-slate-400 py-0.5 px-2 focus:outline-none focus:border-sky-500'}),
        }


class UpdatePostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'title_tag', 'body')

        widgets = {
            'title': forms.TextInput(attrs={'class': 'w-80 block mb-8 border-b-2 border-slate-400 py-0.5 px-2 focus:outline-none focus:border-sky-500'}),
            'title_tag': forms.TextInput(attrs={'class': 'w-80 block mb-8 border-b-2 border-slate-400 py-0.5 px-2 focus:outline-none focus:border-sky-500'}),
            'body': forms.Textarea(attrs={'class': 'w-80 block mb-8 border-2 border-slate-400 py-0.5 px-2 focus:outline-none focus:border-sky-500'}),
        }